import java.io.Serializable;

/**
 * A class representing a player of the game
 * @author Gavin Kremer
 */
public class Player implements Serializable{

    /**
     * Numerical value representing the affinity of the player.
     */
    private int affinity;

    /**
     * Constructor that assigns a value to {@link #affinity}
     * @param affinity value for {@link #affinity}
     */
    Player(int affinity){
        this.affinity = affinity;
    }

    /**
     * Getter for {@link #affinity}
     * @return value of affinity
     */
    public int getAffinity() {
        return affinity;
    }

    /**
     * Setter for {@link #affinity}
     * @param affinity new value for affinity
     */
    public void setAffinity(int affinity) {
        this.affinity = affinity;
    }
}
