import java.io.*;

/**
 * File manager for saving and loading.
 * @author Gavin Kremer
 */
public class FileManager {

    /**
     * This method creates a save file by saving the board to a file specified by the UI.
     * @param save filename to save to
     * @param obj object to save
     */
    public static <T> boolean writeSave(String save, T obj){
        try {
            File saves = new File("ReversiSaves");
            if(!saves.exists()){
                try{
                    saves.mkdir();
                }
                catch(SecurityException se){
                    throw new SecurityException("Could not create the directory!");
                }
            }
            FileOutputStream fos = new FileOutputStream("ReversiSaves/"+save+".rs");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(obj);
            oos.close();
        }
        catch(IOException X){
            return false;
        }
        return true;
    }

    /**
     * This method finds and returns a saved board inside the given file.
     * @param save file to load from
     * @return board found.
     */
    public static <T> T readSave(String save){
        try{
            FileInputStream fis = new FileInputStream("ReversiSaves/"+save+".rs");
            ObjectInputStream ois = new ObjectInputStream(fis);
            return (T) ois.readObject();
        }
        catch(Exception X){
            return null;
        }
    }
}
