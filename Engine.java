/**
 * The engine of the game that makes everything happen.
 * It's the heart <3
 * @author Gavin Kremer
 */
public class Engine {

    /**
     * This represents the {@link Board} that the
     * engine will be manipulating.
     */
    private Board board;

    /**
     * This is the implementation of {@link UI} that the
     * game will use to communicate with the players.
     */
    private UI ui;

    /**
     * Represents if the game is being started from a save
     * file.
     */
    private boolean fromSave;

    /**
     * An array used for hashing colors with affinity values.
     */
    private String[] colors = {"NONE","BLUE","RED","GREEN","YELLOW","MAGENTA"};

    /**
     * Constructor for this class that should be used when
     * making a new game.
     *
     * @param userInt value for {@link #ui}
     */
    Engine(UI userInt) {
        this.ui = userInt;
        init(null);
    }

    /**
     * Constructor for this class to be used when loading
     * a previous game.
     *
     * @param userInt value of {@link #}
     * @param save    saved board.
     */
    Engine(UI userInt, Board save) {
        this.ui = userInt;
        init(save);
    }

    /**
     * Initializes the game.
     * Places the tiles in the appropriate spots
     * and then displays the board for the first time.
     * @param board board to be used in the game.
     */
    private void init(Board board) {
        if (board == null) {
            this.board = new Board();
        } else {
            this.board = board;
        }
        this.board.setPlayer(ui.makePlayer(0), 0);
        this.board.setPlayer(ui.makePlayer(this.board.getPlayer(0).getAffinity()), 1);
        this.board.getTileAt(3, 3).setAffinity(this.board.getPlayer(0).getAffinity());
        this.board.getTileAt(3, 4).setAffinity(this.board.getPlayer(1).getAffinity());
        this.board.getTileAt(4, 3).setAffinity(this.board.getPlayer(1).getAffinity());
        this.board.getTileAt(4, 4).setAffinity(this.board.getPlayer(0).getAffinity());
        ui.displayBoard(this.board, colors[this.board.getPlayer(0).getAffinity()], colors[this.board.getPlayer(1).getAffinity()],2,2);
        Player winner = gameLoop(0);
        ui.displayMessage(colors[winner.getAffinity()]+" has won the game!");

    }

    /**
     * The game loop that runs until the game ends or is stopped.
     * @param moves keeps track of the amount of times a move was impossible,
     * which if greater than two, will cause the game to end.
     * @return the winning {@link Player}
     */
    private Player gameLoop(int moves) {
        int noMoves = moves;
        int onecount = countPieces(board.getPlayer(0).getAffinity());
        int twocount = countPieces(board.getPlayer(1).getAffinity());

        //Checks if more moves are possible and if not ends the game.
        if(noMoves > 1){
            ui.displayMessage("No more moves are possible!");
            return onecount > twocount ? board.getPlayer(0): board.getPlayer(1);
        }

        //Basic loop that allows each player to take a turn.
        for (int i = 0; i <= 1; i++) {
            Player player = board.getPlayer(i);
            boolean hasMove = false;
            boolean hasPieces = false;
            boolean[][] available = new boolean[8][8];
            //This loop traverses through each tile on the board.
            for(int x = 0; x <= 7; x++){
                for(int l = 0; l <= 7; l++){
                    int[] coords = {x,l};
                    //This ensures the player still has pieces.
                    if(board.getTileAt(x,l).getAffinity() != 0 && board.getTileAt(x,l).getAffinity() != player.getAffinity()){
                        hasPieces = true;
                    }
                    //This ensures the player is able to move at the given spot.
                    if(checkSurroundings(player,coords) != 0){
                        available[x][l] = true;
                        hasMove = true;
                    }
                    else{
                        available[x][l] = false;
                    }
                }
            }

            //This causes the game to end if the player has no remaining pieces.
            if(!hasPieces){
                ui.displayMessage(colors[player.getAffinity()]+" has no pieces and lost the game!");
                if(i == 0){
                    return board.getPlayer(1);
                }
                else{
                    return board.getPlayer(0);
                }
            }

            //This is the turn process that occurs if the player is able to move.
            if(hasMove) {
                int[] coords = null;
                ui.displayMessage("Pick a spot to place a token, " + colors[player.getAffinity()]);
                int successes = 0;
                boolean correct = false;
                while (!correct) {
                    coords = ui.chooseTile(player);
                    try {
                        correct = available[coords[0]][coords[1]];
                    }
                    catch(ArrayIndexOutOfBoundsException x){
                        //I don't know what else to put here cause I'm a bad programmer LOL.
                    }
                    if (!correct) {
                        ui.displayMessage("You cannot put a token there.");
                    }
                    //successes = checkSurroundings(player,coords);
                }
                successes = changeAffinities(player, coords);
                noMoves = 0;
            }

            //This happens if the player cannot move this turn.
            else{
                ui.displayMessage(colors[player.getAffinity()]+" has no moves and must forfeit the turn!");
                noMoves += 1;
            }

            //This recounts all of the pieces on the board to be displayed with the board.
            onecount = countPieces(board.getPlayer(0).getAffinity());
            twocount = countPieces(board.getPlayer(1).getAffinity());
            ui.displayBoard(board,colors[board.getPlayer(0).getAffinity()], colors[board.getPlayer(1).getAffinity()],onecount,twocount);
        }
        //Restarts the process.
        return gameLoop(noMoves);
    }

    /**
     * Pretty intuitive, this counts up all the pieces on the board that has the
     * affinity value as the given int
     * @param affinity affinity to search for
     * @return the amount of pieces with said affinity.
     */
    private int countPieces(int affinity){
        int count = 0;
        for(int z = 0; z <= 7; z++){
            for(int y = 0; y <= 7; y++){
                if(board.getTileAt(z,y).getAffinity() == affinity){
                    count++;
                }
            }
        }
        return count;
    }

    /**
     * Checks the surroundings of the given coordinates and identifies legal rows
     * if any.
     * @param p player to search relative to.
     * @param coords the coordinates to place at.
     * @return the amount of possible rows.
     */
    private int checkSurroundings(Player p,int[] coords){
        int successes = 0;
        if (checkValidity(p, coords, 1, 0)) successes += 1;
        if (checkValidity(p, coords, -1, 0)) successes += 1;
        if (checkValidity(p, coords, -1, 1)) successes += 1;
        if (checkValidity(p, coords, 1, -1)) successes += 1;
        if (checkValidity(p, coords, -1, -1)) successes += 1;
        if (checkValidity(p, coords, 1, 1)) successes += 1;
        if (checkValidity(p, coords, 0, 1)) successes += 1;
        if (checkValidity(p, coords, 0, -1)) successes += 1;
        return successes;
    }

    /**
     * Checks the validity of a certain row.
     * @param p player to search relative to.
     * @param coords coordinates to start at.
     * @param x change in x coordinate
     * @param y change in y coordinate
     * @return if the row is a valid overturn.
     */
    private boolean checkValidity(Player p, int[] coords, int x, int y) {
        boolean terminated = false;
        boolean start = false;
        if(board.getTileAt(coords[0],coords[1]).getAffinity() != 0){
            return false;
        }
        int[] sentinel = {coords[0] + x, coords[1] + y};
        while (sentinel[0] <= 7 && sentinel[0] >= 0 && sentinel[1] <= 7 && sentinel[1] >= 0 && !terminated) {
            if (board.getTileAt(sentinel[0], sentinel[1]).getAffinity() == 0) {
                return false;
            } else if (board.getTileAt(sentinel[0], sentinel[1]).getAffinity() == p.getAffinity()) {
                terminated = true;
            } else {
                start = true;
                sentinel[0] += x;
                sentinel[1] += y;
            }
        }
        return (start && terminated);
    }

    /**
     * Changes the affinities of each row including the spot the token is placed.
     * @param p player to change relative to.
     * @param coords coordinates to start at
     * @return amount of rows changed.
     */
    private int changeAffinities(Player p, int[] coords){
        int successes = 0;
        if (changeAffinities(p, coords, 1, 0)) successes++;
        if (changeAffinities(p, coords, -1, 0)) successes++;
        if (changeAffinities(p, coords, -1, 1)) successes++;
        if (changeAffinities(p, coords, -1, -1)) successes++;
        if (changeAffinities(p, coords, 0, 1)) successes++;
        if (changeAffinities(p, coords, 0, -1)) successes++;
        if (changeAffinities(p, coords, 1, 1)) successes++;
        if (changeAffinities(p, coords, 1, -1)) successes++;
        board.getTileAt(coords[0], coords[1]).setAffinity(p.getAffinity());
        return successes;
    }

    /**
     * Changes the affinity of a particular row.
     * @param p player to change relative to.
     * @param coords coordinates to start at.
     * @param x change in x coordinate
     * @param y change in y coordinate
     * @return if the affinity was changed.
     */
    private boolean changeAffinities(Player p, int[] coords, int x, int y) {
        if (checkValidity(p, coords, x, y)) {
            int[] sentinel = {coords[0] + x, coords[1] + y};
            java.util.Stack<int[]> spots = new java.util.Stack<>();
            while (board.getTileAt(sentinel[0], sentinel[1]).getAffinity() != p.getAffinity()) {
                int[] copy = {sentinel[0], sentinel[1]};
                spots.push(copy);
                sentinel[0] += x;
                sentinel[1] += y;
            }
            while (!spots.isEmpty()) {
                int[] target = spots.pop();
                board.getTileAt(target[0], target[1]).setAffinity(p.getAffinity());
            }
            return true;
        }
        return false;
    }
}
