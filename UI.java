/**
 * An interface for the user interface. (Yo dog...)
 * @author Gavin Kremer
 */
public interface UI {

    /**
     * an intro to the game.
     */
    void intro();

    /**
     * Will go through the process of creating/loading a player.
     * @param bad affinity already taken and unpickable.
     * @return generated player
     */
    Player makePlayer(int bad);

    /**
     * Goes through the process of choosing a tile to play.
     * @return
     */
    int[] chooseTile(Player p);

    /**
     * Displays a message to the players.
     * @param msg message to be displayed.
     */
    void displayMessage(String msg);

    /**
     * Displays the game board to the players.
     * @param board board to be displayed.
     */
    void displayBoard(Board board, String name, String name2, int count, int count2);


}


