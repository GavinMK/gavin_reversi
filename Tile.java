import java.io.Serializable;

/**
 * A tile of the board, an individual space where pieces will
 * be placed and changed.
 * @author Gavin Kremer
 */
public class Tile implements Serializable{

    /**
     * Represents who controls the space.
     * 0 = Neutral
     * 1 = Player 1
     * 2 = Player 2
     */
    private int affinity;

    /**
     * Basic constructor for this class.
     */
    Tile(){
        affinity = 0;
    }

    /**
     * Getter for {@link #affinity}
     * @return value of {@link #affinity}
     */
    public int getAffinity(){
        return affinity;
    }

    /**
     * Sets the value of {@link #affinity}
     * @param affinity new value for {@link #affinity}
     */
    public void setAffinity(int affinity){
        this.affinity = affinity;
    }
}
