import java.io.Serializable;

/**
 * This class represents the board where all of the pieces
 * are played and the game is manipulated. You should know
 * this ._.
 * @author Gavin Kremer
 */
public class Board implements Serializable{

    /**
     * A two dimensional array representing the state
     * of the board, made up of {@link Tile}s
     */
    private Tile[][] state = new Tile[8][8];

    /**
     * An array representing the players of the game.
     */
    private Player[] players = new Player[2];

    /**
     * Default constructor which initializes each tile in
     * the {@link #state}
     */
    Board(){
        for(int i = 0; i<8; i++){
            for(int l = 0; l<8; l++){
                state[i][l] = new Tile();
            }
        }
    }

    /**
     * Gets and returns a tile at the given coordinates.
     * @param x first dimension of the tiles to search
     * @param y second dimension
     * @return tile at the given coordinates, null if out
     * of bounds.
     */
    public Tile getTileAt(int x, int y){
        return (x<=7 && x>=0 && y<=7 && y>=0) ? state[x][y] : null;
    }

    /**
     * Changes the value of {@link #players}
     * @param p {@link Player} to set in a spot
     * @param x index to set the player in.
     */
    public void setPlayer(Player p,int x){
        players[x] = p;
    }

    /**
     * Getter for the value of {@link #players}
     * @param x index to return from.
     * @return found {@link Player}
     */
    public Player getPlayer(int x){
        return players[x];
    }
}
