/**
 * A console implementation of the interface {@link UI}
 * @author Gavin Kremer
 */
public class Console implements UI{

    /**
     * A scanner that takes console input. It's called "swift" just as an old
     * tradition
     */
    private final java.util.Scanner swift = new java.util.Scanner(System.in);

    /**
     * An array that holds token representation..
     */
    private String[] tokens = {" ","\u001B[34m◉\u001B[0m","\u001B[31m◉\u001B[0m","\u001B[32m◉\u001B[0m","\u001B[33m◉\u001B[0m","\u001B[35m◉\u001B[0m"};

    @Override
    public void intro() {
        System.out.println("Now playing Reversi");
    }

    @Override
    public Player makePlayer(int bad) {
        int choice = 0;
        displayMessage("Pick a color:\n1.BLUE\n2.RED\n3.GREEN\n4.YELLOW\n5.MAGENTA");
        while(choice < 1 || choice > 5 || choice == bad) {
            try {
                choice = swift.nextInt();
            }
            catch(Exception X){
                choice = 0;
                displayMessage("Enter only a number corresponding with your choice");
            }
            swift.nextLine();
        }
        return new Player(choice);
    }

    @Override
    public int[] chooseTile(Player p) {
        String choice = null;
        while(choice == null){
            choice = swift.nextLine();
            if(choice.startsWith("!")){
                handleCommand(choice);
                choice = null;
            }
            else if(choice.length() != 2 || !choice.matches("\\d\\D") && !choice.matches("\\D\\d")){
                displayMessage("You must enter the letter and the number of the spot you wish to place a token on.");
                choice = null;
            }
        }
        char row;
        char col;
        if(Character.isDigit(choice.charAt(0))){
            row = choice.charAt(0);
            col = choice.charAt(1);
        }
        else{
            row = choice.charAt(1);
            col = choice.charAt(0);
        }
        int[] coords = {Character.getNumericValue(col)-10,Character.getNumericValue(row)-1};
        return coords;

    }

    private void handleCommand(String command){

    }

    @Override
    public void displayMessage(String msg) {
        System.out.println(msg);
    }

    /**
     * Displays a prompt that the user can agree or disagree with.
     * @param prompt message to ask the player
     * @return if the player chose yes or not.
     */
    public boolean displayPrompt(String prompt) {
        System.out.println(prompt+" (Y/N)");
        String choice = swift.nextLine().toUpperCase();
        return choice.startsWith("Y");

    }

    @Override
    public void displayBoard(Board board, String name, String name2, int count, int count2) {
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("  A  B  C  D  E  F  G  H");
        System.out.print("1");
        for(int i = 0; i <= 7; i++){
            for(int l = 0; l <= 7; l++){
                System.out.print("["+tokens[board.getTileAt(l,i).getAffinity()]+"]");
                if(l == 7 && i != 7){
                    if(i == 2){
                        System.out.print("  "+name+"'s Tokens:");
                    }
                    else if(i == 3){
                        System.out.print("  "+count);
                    }
                    else if( i == 5){
                        System.out.print("  "+name2+"'s Tokens:");
                    }
                    else if (i == 6){
                        System.out.print("  "+count2);
                    }
                    System.out.print("\n"+(i+2));

                }
            }
        }
        System.out.println("\n");


    }
}
